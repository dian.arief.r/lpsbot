<?php
/*
*
* Fungsi Database
*
*/

// masukkan database framework nya
require_once 'medoo.php';

// koneksikan ke database

// ini contoh menggunakan SQLite

  $database = new medoo([
      'database_type' => 'sqlite',
      'database_file' => 'rekening.db',
  ]);

// uncomment ini jika menggunakan mySQL atau mariaDB
// sesuaikan nama database, host, user, dan passwordnya
/*
    $database = new medoo([
        'database_type' => 'mysql',
        'database_name' => 'diary',
        'server' => 'localhost',
        'username' => 'root',
        'password' => 'rahasiakusendiri',
        'charset' => 'utf8'
    ]);
*/

function add($iduser, $pesan)
{
    global $database;
    $last_id = $database->insert('user', [
        'idUsers'    => $iduser,
        'Bank' => $pesan,
    ]);
    return $last_id;
}

// fungsi menghapus diary
function delete($iduser, $idpesan)
{
    global $database;
    $database->delete('user', [
        'AND' => [
            'idUsers' => $iduser,
            'Bank' => $idpesan,
        ],
    ]);
    return '⛔️ telah dilaksanakan..';
}

//cari kode bank
function cariNamaBank($pesan)
{
    global $database;
    $hasil = 'Maaf, Bank yang dicari tidak ditemukan..';
    //$coba = $database->query("SELECT * FROM rekening WHERE Rekening LIKE '%" . $pesan . "%'");
    $datas = $database->select('Bank', [
        'bName',
        'no',
    ], [
        'no' => $pesan,
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $n = 0;
        foreach ($datas as $data) {
            $n++;
            $hasil = $data[bName];
        }
    }
    return $hasil;
}

//cari bank
function cariBank($bank)
{
    global $database;
    $hasil = 'Maaf, Bank yang dicari tidak ditemukan..';
    //$coba = $database->query("SELECT * FROM rekening WHERE Rekening LIKE '%" . $pesan . "%'");
    $datas = $database->select('Bank', [
        'bName',
        'no',
      ],
    [
        'bName[~]' => $bank,
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $hasil = "Kode Bank       Daftar Bank: \n";
        $n = 0;
        foreach ($datas as $data) {
            $n++;
            //$hasil .= $jml;
            $hasil .= "`$data[no]`"."           "."`$data[bName]`\n";
        }
        $hasil .= "\n\nUntuk mengecek status rekening dan nominal penjaminan, silahkan kirim /cek`[spasi]`kode bank`[spasi]` no rekening";
    }
    return $hasil;
}

function carirekening($kode, $rekening)
{
    $namaBank = cariNamaBank($kode);
    global $database;
    $hasil = 'Maaf, data yang dicari tidak ditemukan..';
    //$coba = $database->query("SELECT * FROM rekening WHERE Rekening LIKE '%" . $pesan . "%'");
    $datas = $database->select($namaBank, [
        'Rekening',
        'Status',
        'Nominal'
    ], [
        'Rekening' => $rekening,
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $hasil = "Status rekeningmu: ";
        $n = 0;
        foreach ($datas as $data) {
            $n++;
            //$hasil .= $jml;
            $hasil .= "`$data[Status]`\n"."Nominal Penjaminan: "."`$data[Nominal]`\n";
        }
        $hasil .= "\nSilahkan hubungi kantor BRI terdekat di kota anda.";
    }

//
    return $hasil;
}
